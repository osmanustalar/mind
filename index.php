<?php
require_once('system/core/Mind.php');
$conf = array(
    'host'      =>  'localhost',
    'dbname'    =>  'mind',
    'username'  =>  'root',
    'password'  =>  'password'
);

$Mind = new Mind($conf);

$Mind->route('/', 'app/controllers/ui/HomeController:index');